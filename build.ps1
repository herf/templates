function Build([System.IO.FileInfo] $File)
{
    Write-Output "--- Building $($File.FullName)"

    g++ -std=c++11 $File -o "$($File.BaseName).out"
}

$Sources = Get-ChildItem . -Filter *.cc
foreach ($Source in $Sources)
{
    Build($Source)
}
