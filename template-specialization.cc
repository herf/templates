// Template specialization is a technique when we create a specialized implementation of a generic class template
// An example from the standard library is std::vector<bool>
// When you use std::vector<bool>, the compiler will not instantiate this type,
// because it is already specialized to be space efficient and store the values in bits instead of bool
// So it vanb e helpful, when a certain type requires special handling, but you still want to have
// a generic class to handle the rest of the types
// Also it is transparent to the user, the same class template will be used
#include <iostream>

template<typename T, typename U>
class A
{
public:
    void f()
    {
        std::cout << "A<T, U>::f()" << std::endl;
    }
};

template<typename T, typename U>
void a()
{
    std::cout << "a<T, U>()" << std::endl;
}

// Partial specialization
template<typename U>
class A<int, U>
{
public:
    void f()
    {
        std::cout << "A<int, U>::f()" << std::endl;
    }
};

// Partial specialization is only allowed for classes
// Mostly we can use overloads in this case
// (when the template argument is used in the parameter list)
// or we need to do some hack if it is really needed
/*template<typename U>
void a<int, U>()
{
    std::cout << "a<int, U>()" << std::endl;
}*/

// Full specialization
template<>
class A<int, float>
{
public:
    void f()
    {
        std::cout << "A<int, float>::f()" << std::endl;
    }
};

template<>
void a<int, float>()
{
    std::cout << "a<int, float>()" << std::endl;
}

int main()
{
    A<double, char> a0;
    A<int, int> a1;
    A<int, float> a2;

    a0.f();
    a1.f();
    a2.f();

    a<double, char>();
    a<int, int>();
    a<int, float>();

    return 0;
}
