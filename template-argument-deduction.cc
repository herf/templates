// Argument deduction only applies for function templates of course
#include <iostream>
#include <iomanip>
#include <type_traits>

void print_result(const char* message, bool result)
{
    std::cout <<std::left << std::setw(30) << message << ": " << std::boolalpha << result << std::endl;
}

template<typename Expected, typename T>
void value(T t)
{
    print_result("value<T>(T)", std::is_same<T, Expected>::value);
}

template<typename Expected, typename T>
void reference(T& t)
{
    print_result("reference<T>(T&)", std::is_same<T, Expected>::value);
}

template<typename Expected, typename T>
void const_reference(const T& t)
{
    print_result("const_reference<T>(cosnt T&)", std::is_same<T, Expected>::value);
}

template<typename Expected, typename T>
void forward_reference(T&& t)
{
    print_result("forward_reference<T>(T&&)", std::is_same<T, Expected>::value);
}

template<typename Expected, typename T>
void pointer(T* t)
{
    print_result("pointer<T>(T*)", std::is_same<T, Expected>::value);
}

int main()
{
    int i = 5;
    const int ci = 5;

    // --------------------------
    // Test with hard coded value
    // --------------------------

    // Argument type: int
    // Deduced parameter type: T -> int
    value<int>(5);

    // This will not compile
    // Argument: type: int
    // Deduced parameter type: T& -> int&
    // But you cannot assign a non const reference to an rvalue
    //reference<int&>(5);

    // Argument type: int
    // Deduced parameter type: const T& -> const int&
    const_reference<int>(5);

    // Argument type: int
    // Deduced parameter type: T&& -> int
    forward_reference<int>(5);

    // This will not compile
    // Argument type: int
    // Deduced parameter type: T* -> int*
    // You cannot assign an int to an int*
    // pointer(5);

    // ----------------------------
    // Test with non-const variable
    // ----------------------------

    // Argument type: int&
    // Deduced parameter type: T-> int
    value<int>(i);

    // Argument type: int&
    // Deduced parameter type: T& -> int&
    reference<int>(i);

    // Argument type: int&
    // Deduced parameter type: const T& -> const int&
    const_reference<int>(i);

    // Argument type: int&
    // Deduced parameter type: T&& -> int&
    forward_reference<int&>(i);

    // This will not compile
    // Argument type: int&
    // Deduced parameter type: T* -> int*
    // You cannot assign an int& to an int*
    // pointer(i);

    // ------------------------
    // Test with const variable
    // ------------------------

    // Argument type: const int&
    // Deduced parameter type: T-> int
    value<int>(ci);

    // Argument type: const int&
    // Deduced parameter type: T& -> const int&
    reference<const int>(ci);

    // Argument type: const int&
    // Deduced parameter type: const T& -> const int&
    const_reference<int>(ci);

    // Argument type: const int&
    // Deduced parameter type: T&& -> const int&
    forward_reference<const int&>(ci);

    // This will not compile
    // Argument type: const int&
    // Deduced parameter type: T* -> const int*
    // You cannot assign a const int& to a const int*
    // pointer(ci);

    // ---------------------------
    // Test with non-const pointer
    // ---------------------------

    // Argument type: int*
    // Deduced parameter type: T -> int*
    value<int*>(&i);

    // This will not compile
    // Argument type: int*
    // Deduced parameter type: T& -> int*&
    // You cannot assign rvalue to a non const reference
    // reference<int*>(&i);

    // Argument type: int*
    // Deduced parameter type: const T& -> const int*&
    const_reference<int*>(&i);

    // Argument type: int*
    // Deduced parameter type: T&& -> int*
    forward_reference<int*>(&i);

    // Argument type: int*
    // Deduced parameter type: T* -> int*
    pointer<int>(&i);

    // -----------------------
    // Test with const pointer
    // -----------------------

    // Argument type: const int*
    // Deduced parameter type: T -> const int* // Only "top level" consts are removed
    value<const int*>(&ci);

    // This will not compile
    // Argument type: const int*
    // Deduced parameter type: T& -> const int*&
    // You cannot assign rvalue to a non const reference
    // reference<const int*>(&ci);

    // Argument type: const int*
    // Deduced parameter type: const T& -> const int*&
    const_reference<const int*>(&ci);

    // Argument type: const int*
    // Deduced parameter type: T&& -> const int*
    forward_reference<const int*>(&ci);

    // Argument type: const int*
    // Deduced parameter type: T* -> const int*
    pointer<const int>(&ci);

    return 0;
}