#include <tuple>
#include <iostream>

template<int Arg, int... Args>
class Add
{
public:
    static constexpr int value = Arg + Add<Args...>::value;
};

template<int Arg>
class Add<Arg>
{
public:
    static constexpr int value = Arg;
};

template<typename... Args>
class Tuple
{
public:
    Tuple(Args... args): m_Tuple(args...)
    {
        // Nothing to do yet
    }

    template<typename T>
    void foreach(T&& t)
    {
        foreach_impl(std::forward<T>(t), typename SequenceGenerator<sizeof...(Args)>::type());
    }

private:
    template<int... Integers>
    class Sequence
    {

    };
    template<int Length, int... Integers>
    class SequenceGenerator: public SequenceGenerator<Length - 1, Length - 1, Integers...>
    {

    };

    template<int... Integers>
    class SequenceGenerator<0, Integers...>
    {
    public:
        using type = Sequence<Integers...>;
    };

    template<typename T, int... Integers>
    void foreach_impl(T&& t, Sequence<Integers...> /*sequence*/)
    {
        int dummy[sizeof...(Integers)] = { (t(std::get<Integers>(m_Tuple)), 0)... };
    }


    std::tuple<Args...> m_Tuple;
};

template<typename... Args>
void f(Args... args)
{
    std::tuple<Args...>(args...);
}

template<typename... Args>
void g(Args... args)
{
    f(std::tuple<Args>(args)...);
}

class Callback
{
public:
    template<typename T>
    void operator()(T& t)
    {
        std::cout << t << std::endl;
    }
};

int main()
{
    f(5, 6.0f, "hello");
    constexpr int s = Add<5, 6, 7, 8>::value;
    std::cout << s << std::endl;
    Tuple<int, float, char> tuple(5, 6.0f, '7');
    tuple.foreach(Callback());

    return 0;
}
