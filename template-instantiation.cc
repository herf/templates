// The compiler will not be able to create binary for the class template when it sees its definition
// All the template arguments must be provided to have the complete type,
// so then the compiler will be able to create the machine code for the template class
// This is why the whole implementation of the class template needs to be in header files!

template<typename T>
class A
{
public:
    void f()
    {
        T t; // Assuming T is default constructible

        t.f(); // Assuming T has a member f()
    }

    void g()
    {
        T t;

        t.g();
    }

    // This will cause compile time error even without instantiating a class from this template
    // because nothing in this function is dependent on the template parameter,
    // so the compiler is able to generate maxchine code for this
    // So actually - if it was correct- this method could be defined in a source file
    /*void h()
    {
        int i;

        i.h();
    }*/
};

class Impl
{
public:
    void f()
    {

    }
};

int main()
{
    A<Impl> a;

    // A<T>::g() is not compiled, since it is not used
    // See: https://godbolt.org/z/SzZdsZ
    // So sometimes, templates can even ending up having smaller executable
    // since not used code is not even compiled
    a.f();

    return 0;
}
