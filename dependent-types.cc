#include <iostream>
#include<vector>

template<typename T>
class Base
{
    public:
        using PointerType = T*;

        void f() const
        {
            std::cout << "Base<T>::f() const" << std::endl;
        }

        template<typename U>
        void ff() const
        {
            std::cout << "Base<T>::ff<U>() const" << std::endl;
        }
};

template<typename T>
class Child: public Base<T>
{
    public:
        void g() const
        {
            typename Base<T>::PointerType pointer = nullptr;

            // This will not compile, since f() is defined in the base class,
            // the base class' type is dependent on the type template parameter 'T'
            // Since calling f() is not dependent on any template parameter,
            // the compiler will try to create code for this part of the class.
            // So we need to make the calling of f() dependent on the template parameter
            // This can be done, by invoking f() with the this pointer
            // f();
            this->f();
            // This will not compile either, since the compiler needs to know what we expect ff to be
            // By using this, we made the call dependent on the template parameter, 
            // but the compiler has no way to know what ff will be when the class template is instantiated
            // So we need to tell, by either typing typename or template
            // this->ff<T>();
            this->template ff<T>();
            std::cout << "Child<T>::g() const" << std::endl;

            // Here, typename is needed again, since std::vector<T> is a dependent type
            typename std::vector<T>::value_type value;
        }

    private:
        std::vector<T> m_Vector;;
};

int main()
{
    Child<int> child;

    child.g();

    return 0;
}
