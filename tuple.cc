#include <tuple>
#include <type_traits>
#include <iostream>

template<typename T, typename First, typename... Args>
struct CountImpl
{
    static constexpr size_t value = (std::is_same<T, First>::value ? 1 : 0) + CountImpl<T, Args...>::value;
};

template<typename T, typename First>
struct CountImpl<T, First>
{
    static constexpr size_t value = std::is_same<T, First>::value ? 1 : 0;
};

template<typename T, typename... Args>
constexpr size_t Count(const std::tuple<Args...>& tuple)
{
    return CountImpl<T, Args...>::value;
}

template<size_t Index, typename T, typename First, typename... Args>
struct IndexImpl
{
    static constexpr size_t value = std::is_same<T, First>::value ? Index : IndexImpl<Index + 1, T, Args...>::value;
};

template<size_t Index, typename T, typename First>
struct IndexImpl<Index, T, First>
{
    static constexpr size_t value = std::is_same<T, First>::value ? Index : Index + 1;
};

template<typename T, typename... Args>
constexpr size_t Index(const std::tuple<Args...>& tuple)
{
    return IndexImpl<0, T, Args...>::value;
}

template<typename T, typename... Args>
constexpr T Get(const std::tuple<Args...>& tuple)
{
    static_assert(Index<T>(tuple) < sizeof...(Args), "The requested type is not in the tuple");
    static_assert(Count<T>(tuple) == 1, "Tuple must have exactly one element with the requested type");
    return std::get<Index<T>(tuple)>(tuple);
}

int main()
{
    std::tuple<int, float, int> tuple_a(1, 2.0f, 3);

    static_assert(Count<int>(tuple_a) == 2, "Count<int>(tuple_a) == 2 FAILED");
    static_assert(Count<float>(tuple_a) == 1, "Count<float>(tuple_a) == 1 FAILED");
    static_assert(Count<double>(tuple_a) == 0, "Count<double>(tuple_a) == 0 FAILED");

    std::cout << "Count<int>(tuple_a):    " << Count<int>(tuple_a) << std::endl;
    std::cout << "Count<float>(tuple_a):  " << Count<float>(tuple_a) << std::endl;
    std::cout << "Count<double>(tuple_a): " << Count<double>(tuple_a) << std::endl;

    std::tuple<int, float, char> tuple_b(5, 6.6f, '7');

    static_assert(Index<int>(tuple_b) == 0, "Index<int>(tuple_b) == 0 FAILED");
    static_assert(Index<float>(tuple_b) == 1, "Index<float>(tuple_b) == 1 FAILED");
    static_assert(Index<char>(tuple_b) == 2, "Index<char>(tuple_b) == 3 FAILED");
    static_assert(Index<double>(tuple_b) == 3, "Index<double>(tuple_b) FAILED");

    std::cout << "Index<int>(tuple_b):    " << Index<int>(tuple_b) << std::endl;
    std::cout << "Index<float>(tuple_b):  " << Index<float>(tuple_b) << std::endl;
    std::cout << "Index<char>(tuple_b):   " << Index<char>(tuple_b) << std::endl;
    std::cout << "Index<double>(tuple_b): " << Index<double>(tuple_b) << std::endl;

    std::cout << Get<int>(tuple_b) << std::endl;
    // This will not compile, since tuple_a has two elements with type int
    // std::cout << Get<int>(tuple_a) << std::endl;
    // This will not compile either, since tuple_b has no double element
    // std::cout << Get<double>(tuple_b) << std::endl;

    return 0;
}
