#include <type_traits>
#include <iostream>

class Base
{

};

class Child: public Base
{

};

int main()
{
    std::cout << std::boolalpha << std::is_same<int, int>::value << std::endl;
    std::cout << std::boolalpha << std::is_same<int, float>::value << std::endl;
    std::cout << std::boolalpha << std::is_base_of<Base, Child>::value << std::endl;
    std::cout << std::boolalpha << std::is_base_of<Base, std::ostream>::value << std::endl;
    std::remove_pointer<int*>::type i = 5;
    std::add_pointer<int>::type ip = &i;

    std::cout << "i: " << i << ", *ip: " << *ip << std::endl;
    return 0;
}
