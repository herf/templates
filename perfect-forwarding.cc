#include <iostream>

template<typename T>
void f(T t)
{

}

// This will not do perfect forwarding, since t is not an rvalue reference inside forward()
// It has a name so it in reality is a T&& &, which will collapse into T&
// Imagine it is invoked with an int&&. In this case T will be int&&, so t will also be an int&&.
// When passed to f() it will became an int && &, and during the deduction it will became int &.
// So instea of moving the value it will be copied
template<typename T>
void bad_forward(T&& t)
{
    f(t);
}

// This will do the job
// static_cast-ing t to T&&, it will become T&& &&, which will collapse into T&&
template<typename T>
void good_forward(T&& t)
{
    f(static_cast<T&&>(t));
}

class C
{
public:
    C()
    {
        std::cout << "C::C()" << std::endl;
    }

    ~C()
    {
        std::cout << "C::~C()" << std::endl;
    }

    C(const C&)
    {
        std::cout << "C::C(const C&)" << std::endl;
    }

    C(C&&)
    {
        std::cout << "C::C(C&&)" << std::endl;
    }
};

int main()
{
    C c;

    std::cout << "----- bad_forward -----" << std::endl;
    // Argument type: C&, T: C&, t: C& && -> C&
    // Calling f:
    // Argument type: C&, T: C, t: C (C& -> C) so it will be copied (and it should be)
    bad_forward(c);
    // Argument type: C&&, T: C&&, t: C&& && -> C&&
    // Calling f:
    // Argument type: C&& & -> C&, T: C, t: C (C& -> C) so it will be copied (and it should not be)
    bad_forward(C());
    std::cout << "----- good_forward -----" << std::endl;
    // Argument type: C&, T: C&, t: C& && -> C&
    // Calling f:
    // Argument type: C&, T: C, t: C (C& -> C) so it will be copied (and it should be)
    good_forward(c);
    // Argument type: C&&, T: C&&, t: C&& && -> C&&
    // Calling f:
    // Argument type: C&& && -> C&&, T: C&&, t: C (C&& -> C) so it will be moved (and it should be)
    good_forward(C());
}