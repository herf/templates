// Shows the usage of different template parameters
#include<array>
#include <vector>
#include <string>

// Non-type template parameter
template<int I>
class A
{
    int m_I = I;
};

template<int I>
void a()
{

}

// Type template parameter
template<typename T>
class B
{
    T m_T;
};

template<typename T>
void b()
{

}

template<typename T, typename U>
class B1
{
    T m_T;
    U m_U;
};

template<typename T, typename U>
void b1(T t, U u)
{

}

// Template template parameter
// The number of "typename" parameters needs to match exacly
template<template<typename> typename T>
class C
{
    T<float> m_T;
};

template<template<typename> typename T>
void c()
{

}

template<template<typename, typename> typename T>
class D
{
    T<float, int> m_T;
};

int main()
{
    A<5> a5;
    A<6> a6;

    // a5 and a6 are two completely different types, so the following will not work
    // a5 = a6;
    // The non-type template parameters must be compile time constants,
    // so the following will not work either
    // int i = 5;
    // A<i> ai;
    // But constexpr variable can work
    constexpr int i = 5;
    A<i> ai;

    // Works similarly with function templates
    a<5>();

    B<int> bi;
    B<float> bf;
    // B<char> is an instantiation of B, so it is a real type, and can be used as a template parameter of B
    B<B<char>> bbc;

    // Again, these are completely different types

    b<int>();
    b1<int, float>(5, 6.0f);
    // In this case the compiler can deduct the template parameters for us
    // so we do not need to specify them
    b1(5, 6.0f);
    // But be careful, these two are not the same
    b1<int, std::string>(5, "Hello");
    b1(5, "Hello");

    C<B> cb;

    // Template template parameters are great when we want a class template to accept other class templates
    // as template parameter
    // This will not work, since std::vector accepts two template arguments (one for the value, one for the allocator)
    // althouth the second one is defaulted
    // C<std::vector> cv;
    // But this will work
    D<B1> dv;
}