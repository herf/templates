#include <iostream>
#include <type_traits>

// Use SFINAE in template parameter list
template<typename T, typename std::enable_if<std::is_integral<T>::value, int>::type = 5>
void f(T t)
{
    std::cout << "f(T) is invoked with an integral type" << std::endl;
}

template<typename T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 5>
void f(T t)
{
    std::cout << "f(T) is invoked with a non-integral type" << std::endl;
}

// Use SFINAE in the argument list
template<typename T>
void g(typename std::enable_if<std::is_integral<T>::value, T>::type t)
{
    std::cout << "g(T) is invoked with an integral type" << std::endl;
}

template<typename T>
void g(typename std::enable_if<!std::is_integral<T>::value, T>::type t)
{
    std::cout << "g(T) is invoked with a non-integral type" << std::endl;
}

// Use SFINAE in return type
template<typename T>
typename std::enable_if<std::is_integral<T>::value>::type h(T t)
{
    std::cout << "h(T) is invoked with an integral type" << std::endl;
}

template<typename T>
typename std::enable_if<!std::is_integral<T>::value>::type h(T t)
{
    std::cout << "h(T) is invoked with a non-integral type" << std::endl;
}

// In c++14, you have a helper type called std::enbale_if_t<condition, type>, which is basiclally just a type alias for std::enable_if<condition, type>::type
// It can help to make the code more readable, becuase std::enable_if is ugly

int main()
{
    f(5);
    f('5');
    f(6.0f);
    f(6.0);
    // It should work, but looks like it is too complex for the compiler to deduce the argument type
    /*g(5);
    g('5');
    g(6.0f);
    g(6.0);*/
    h(5);
    h('5');
    h(6.0f);
    h(6.0);
    // You can still force it to use the integral version with a non-integral parameter
    f<int>(6.0f);
    g<int>(6.0f);
    h<int>(6.0f);

    return 0;
}
